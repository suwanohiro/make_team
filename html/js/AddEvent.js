class AddEvent {
    static onload() {
        this.InputRange();
        this.Button();
    }

    static InputRange() {
        var nop = document.getElementById("nop");
        var NPPTD = document.getElementById("NPPTD");
        var NTD = document.getElementById("NTD");

        nop.addEventListener("input", function () {
            var nop = document.getElementById("nop");
            var nop_output = document.getElementById("nop_output");

            nop_output.innerHTML = `${nop.value}人`;
            AddElement.AddExcludedNumber();
        });

        NPPTD.addEventListener("input", function () {
            var NPPTD = document.getElementById("NPPTD");
            var NPPTD_output = document.getElementById("NPPTD_output");
            var NTD = document.getElementById("NTD");
            var NTD_output = document.getElementById("NTD_output");

            if (NPPTD.value > 0) {
                NPPTD_output.innerHTML = `${NPPTD.value}人`;
                NTD.value = 0;
                NTD_output.innerHTML = "無効";
            }
            else NPPTD_output.innerHTML = "無効";
        });

        NTD.addEventListener("input", function () {
            var NTD = document.getElementById("NTD");
            var NTD_output = document.getElementById("NTD_output");
            var NPPTD = document.getElementById("NPPTD");
            var NPPTD_output = document.getElementById("NPPTD_output");

            if (NTD.value > 0) {
                NTD_output.innerHTML = `${NTD.value}チーム`;
                NPPTD.value = 0;
                NPPTD_output.innerHTML = "無効";
            }
            else NTD_output.innerHTML = "無効";
        });
    }

    static Button() {
        var AllSelect = document.getElementsByClassName("AllSelect");

        AllSelect[0].addEventListener("click", function () {
            ButtonFunction.AllSelect(true);
        });

        AllSelect[1].addEventListener("click", function () {
            ButtonFunction.AllSelect(false);
        });
    }
}