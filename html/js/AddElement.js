class AddElement {
    static onload() {
        this.AddExcludedNumber();
    }

    // 除外番号の項目を追加
    static AddExcludedNumber() {
        var ExcludedNumber = document.getElementById("ExcludedNumber");
        var nop = document.getElementById("nop");

        ExcludedNumber.innerHTML = "";

        for (var a = 0; a < nop.value; a++) {
            var label = document.createElement("label");
            label.id = `checkbox_label_${a}`;
            label.className = "checkbox_label";
            label.style.display = "block";
            ExcludedNumber.appendChild(label);

            var output = document.getElementById(`checkbox_label_${a}`);

            var input = document.createElement("input");
            input.type = "checkbox";
            input.id = `checkbox_input_${a}`;
            input.className = "checkbox_input";
            output.appendChild(input);

            var span = document.createElement("span");
            span.innerHTML = `${('00' + (a + 1)).slice(-2)} 番`;
            output.appendChild(span);
        }
    }
}